import { LitElement, html } from "lit-element";

class PersonaFichaListado2 extends LitElement {

    static get properties() {
        return {
            fname: {type: String}
        }
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <a @click="${this.moreInfo}" class="list-group-item list-group-item-action" data-bs-toggle="list" role="tab">${this.fname}</a>
        `;
    }

    moreInfo(e) {
        console.log("moreInfo");
        console.log("Se ha pedido más información de la persona " + this.fname);

        this.dispatchEvent(
            new CustomEvent("info-person", {
                detail: {
                    name: this.fname
                }
            })
        );
    }

    deletePerson(e) {
        console.log("delete-person en persona-ficha-listado");
        console.log("Se va a borrar la persona de nombre " + this.fname);

        this.dispatchEvent(
            new CustomEvent("delete-person", {
                detail: {
                    name: this.fname
                }
            })
        )
    }
}

customElements.define("persona-ficha-listado2", PersonaFichaListado2);