import { LitElement, html } from "lit-element";
import "../persona-ficha-listado/persona-ficha-listado.js";
import "../persona-form/persona-form.js";
import "../persona-main-dm/persona-main-dm.js";

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm:{type: Boolean},
            yearsInCompanyFilter: {type: Text}
        };
    }

    constructor() {
        super();

        this.people = [];
        this.showPersonForm = false;
        this.yearsInCompanyFilter = "100";
        
    }

    render() {
 
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>

            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.filter(
                        person => person.yearsInCompany <= parseInt(this.yearsInCompanyFilter)
                    ).map(
                        person => html`<persona-ficha-listado class="col"
                        profile="${person.profile}"
                        fname="${person.name}" 
                        yearsInCompany="${person.yearsInCompany}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}">
                        </persona-ficha-listado>
                        `
                    )}
                </div>
            </div>

            <div class="row">
                <persona-form 
                    @persona-form-store="${this.personaFormStore}"
                    @persona-form-close="${this.personFormClose}" 
                    class="d-none border rounded border-primary" id="personForm">
                </persona-form>
            </div>

            <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>
        `;
    }

    // Se ejecuta al cambiar el valor de alguna de las propiedades
    updated(changedProperties) {
        console.log("updated");

        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if (this.showPersonForm === true) {
                this.showPersonData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people", {
                detail: {
                    people: this.people
                }
            }));
        }
    }

    personaFormStore(e) {
        console.log("personaFormStore");
        console.log("Se va almacenar una persona");

        console.log("La propiedad name de person vale " + e.detail.person.name);
        console.log("La propiedad profile de person vale " + e.detail.person.profile);
        console.log("La propiedad yearsInCompany de person vale " + e.detail.person.yearsInCompany);

        // *** Actualización ***
        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar la perona de nombre " + e.detail.person.name);

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person
            );

            // Esto lo borramos porque al no haber asignación del array person
            // no se ejecutaba la función updated
            /*
             let indexOfPerson = 
                this.people.findIndex(
                    person => person.name === e.detail.person.name
                );

            if (indexOfPerson >= 0) {
                console.log("Persona encontrada");
                this.people[indexOfPerson] = e.detail.person;
            } */

        // *** Borrado ***
        } else {
            console.log("Se va a almacenar una perona nueva");

            // Esto lo borramos porque al no haber asignación del array person
            // no se ejecutaba la función updated
            //this.people.push(e.detail.person);

            // Con los tres punto (...) se descompone el array en componentes
            // en vez de incluir todo el array entero
            this.people = [...this.people, e.detail.person];
        }

        console.log("Proceso terminado. Persona guardada o actualizada");

        this.showPersonForm = false;
    }

    personFormClose(e) {
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de persona");

        this.showPersonForm = false;
    }

    // Activamos la vista de personForm
    showPersonData() {
        console.log("showPersonData");
        console.log("Mostramos el formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");        
    }

    // Activamos la vista de peopleList
    showPersonList() {
        console.log("showPersonList");
        console.log("Mostramos listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    deletePerson(e) {
        console.log("deletePerson en perona-main");
        console.log(e);
        console.log("La persona que se va a borrar es " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e) {
        console.log("infoPerson");
        console.log("Se ha pedido más información de la persona " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }

    peopleDataUpdated(e) {
        console.log("peopleDataUpdated");

        this.people = e.detail.people;
    }
}

customElements.define("persona-main", PersonaMain);