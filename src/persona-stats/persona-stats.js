import { LitElement, html } from "lit-element";

class PersonaStats extends LitElement {

    static get properties() {
        return {
          people: {type: Array}
        }
    }

    constructor() {
        super();

        this.people = [];
    }

    updated(changeProperties) {
      console.log("updated en persona-stats");
      console.log(changeProperties);

      if (changeProperties.has("people")) {
        console.log("Ha cambiado el valor de la propiedas persona en persona-stats");

        let peopleStats = this.gatherPeopleArrayInfo(this.people);

        this.dispatchEvent(new CustomEvent("updated-people-stats", {
          detail: {
            peopleStats: peopleStats
          }
        }));
      }
    }

    gatherPeopleArrayInfo(people) {
      console.log("gatherPeopleArrayInfo");

      let peopleStats = {};
      peopleStats.numberOfPeople = people.length;

      let maxYearsInCompany = 0;
      this.people.forEach(
          person => {
              if (person.yearsInCompany > maxYearsInCompany) {
                  maxYearsInCompany = person.yearsInCompany;
              }
          }
      )
      peopleStats.maxYearsInCompany = maxYearsInCompany;
      console.log("maxYearsInCompany: " + maxYearsInCompany);

      return peopleStats;
    }
}

customElements.define("persona-stats", PersonaStats);