import { LitElement, html } from "lit-element";

class PersonaMainDM extends LitElement {

    static get properties() {
        return {
            people: {type: Array} 
        }
    }

    constructor() {
        super();

        this.people = [
            {
                name: "Cristina Pedroche",
                yearsInCompany: 3,
                photo: {
                    src: "./img/persona7.jpg",
                    alt: "Cristina Pedroche"
                },
                profile: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga."
            }, {
                name: "Bruce Springsteen",
                yearsInCompany: 32,
                photo: {
                    src: "./img/persona2.jpg",
                    alt: "Bruce Springsteen"
                },
                profile: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            }, {
                name: "Bob Dylan",
                yearsInCompany: 30,
                photo: {
                    src: "./img/persona4.jpg",
                    alt: "Bob Dylan"
                },
                profile: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo."
            }, {
                name: "Julianne Phillips",
                yearsInCompany: 5,
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "Julianne Phillips"
                },
                profile: "Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem."
            }, {
                name: "Lila Morillo",
                yearsInCompany: 8,
                photo: {
                    src: "./img/persona1.jpg",
                    alt: "Lila Morillo"
                },
                profile: "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
            }, {
                name: "Elisa Pérez",
                yearsInCompany: 10,
                photo: {
                    src: "./img/persona0.jpg",
                    alt: "Elisa Pérez"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."
            }
        ]
    }

    // Se ejecuta al cambiar el valor de alguna de las propiedades
    updated(changedProperties) {
      console.log("updated");

      if (changedProperties.has("people")) {
        console.log("Ha cambiado el valor de la propiedad people en persona-main-dm")

        this.dispatchEvent(new CustomEvent("people-data-updated", 
            {
                detail: {
                    people: this.people
                }
            }
        ));

      }

    }
}

customElements.define("persona-main-dm", PersonaMainDM);