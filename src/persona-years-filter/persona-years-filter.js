import { LitElement, html } from "lit-element";

class PersonaYearsFilter extends LitElement {

    static get properties() {
        return {
            yearsInCompanyFilter: {type: Text},
            maxValue: {type: Text}
        }
    }

    constructor() {
        super();

        this.yearsInCompanyFilter = "100";
        this.maxValue = "100";
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

            <div>
                <label class="w-100">Años en la empresa: <strong>${this.yearsInCompanyFilter}</strong></label>
                <input class="w-100"
                    type="range" max="${this.maxValue}"
                    value="${this.yearsInCompanyFilter}"
                    @input="${this.updateYearsInCompanyFilter}"
                />
            </div>
        `;
    }

    updateYearsInCompanyFilter(e) {
        console.log("updateYearsInCompanyFilter en persona-years-filter");
        console.log("Actualizamos el valor de la propiedad yearsInCompanyFilter con valor " + e.target.value);
        this.yearsInCompanyFilter = e.target.value;

        this.dispatchEvent(new CustomEvent("update-years-filter", {
            detail: {
                yearsInCompanyFilter: e.target.value
            }
        }));
    }
}

customElements.define("persona-years-filter", PersonaYearsFilter);