import { LitElement, html } from "lit-element";

class TestApi extends LitElement {

    static get properties() {
        return {
            movies: {type: Array}
        }
    }

    constructor() {
        super();

        this.movies = [];
        this.getMovieData();
    }

    render() {
        return html`
            <div>**Inicio**</div>
            ${this.movies.map(
                movie => html`<div>La película ${movie.title} fue dirigida por ${movie.director}</div>`
            )}
            <div>**Fin**</div>
        `;
    }

    getMovieData() {
        console.log("getMovieData");
        console.log("Obteniendo datos de las películas");

        let xhr = new XMLHttpRequest();

        // Al ser una request asincrona este onload se ejecutará cuando el send (que está más
        // abajo) se complete (responda el API)
        xhr.onload = () => {
            console.log("Status: ", xhr.status)
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");

                //console.log("xhr: ", xhr);
                let APIResponse = JSON.parse(xhr.responseText);

                //console.log("APIResponse: ", APIResponse);
                this.movies = APIResponse.results;
            }
        };
        xhr.open("GET","https://swapi.dev/api/films/");
        xhr.send();

        console.log("Fin de getMovieData");
    }
}

customElements.define("test-api", TestApi);