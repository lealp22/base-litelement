import { LitElement, html } from "lit-element";
import "../persona-years-filter/persona-years-filter.js";

class PersonaSidebar extends LitElement {

    static get properties() {
        return {
            peopleStats: {type: Object}
        }
    }

    constructor() {
        super();

        this.peopleStats = {};
    }

    render() {
        console.log("Render persona-sidebar");
        console.log(this.peopleStats);
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <aside>
                <section>
                    <div>
                        Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
                    </div>
                    <br />
                    <persona-years-filter 
                        @update-years-filter="${this.updateYearsInCompanyFilter}"
                        yearsInCompanyFilter="${this.peopleStats.maxYearsInCompany}"
                        maxValue="${this.peopleStats.maxYearsInCompany}">
                    </persona-years-filter>
                    <div class="mt-5">
                        <button @click="${this.newPerson}" class="w-100 btn bg-success" style="font-size: 50px">
                        <strong>+</strong>
                        </button>
                    </div>
                </section>
            </aside>
        `;
    }

    // Función que se ejecuta cuando se modifica el valor del filtro para años en la empresa
    updateYearsInCompanyFilter(e) {
        console.log("updateYearsInCompanyFilter en persona-sidebar");
        console.log(e.detail);
        console.log("Se ha actualizado el filtro a " + e.detail.yearsInCompanyFilter);

        this.dispatchEvent(new CustomEvent("update-years-filter", {
            detail: {
                yearsInCompanyFilter: e.detail.yearsInCompanyFilter
            }
        }));
    }

    // Función que se ejecutar con el evento click sobre el botón
    // Lanza el evento new-person que deberá ser capturado donde se utilice el 
    // elemento persona-sidebar
    newPerson(e) {
        console.log("newPerson");
        console.log("Se va a crear una nueva persona");

        this.dispatchEvent(new CustomEvent("new-person", {}));
    }
}

customElements.define("persona-sidebar", PersonaSidebar);