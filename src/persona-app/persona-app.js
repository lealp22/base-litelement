import { LitElement, html } from "lit-element";
import "../persona-header/persona-header.js";
import "../persona-main/persona-main.js";
import "../persona-footer/persona-footer.js";
import "../persona-sidebar/persona-sidebar.js";
import "../persona-stats/persona-stats.js"

class PersonaApp extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        }
    }

    constructor() {
        super();

        this.people = [];
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <persona-header></persona-header>
            <br /><br /><br />
            <div class="row">
                <persona-sidebar 
                    @new-person="${this.newPerson}" 
                    @update-years-filter="${this.updateYearsInCompanyFilter}"
                    class="col-2">
                </persona-sidebar>
                <persona-main 
                    @updated-people="${this.updatedPeople}" 
                    class="col-10">
                </persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
        `;
    }

    // Se ejecuta al actualizar alguna de las propiedades
    updated(changedProperties) {
        console.log("updated en persona-app");
        console.log(changedProperties);

        if (changedProperties.has("people")) {
            console.log("Ha cambiado la propieda people en persona-app");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    // Función que se ejecuta al capturar el evento new-person de persona-sidebar
    newPerson(e) {
        console.log("newPerson en person-app");
        // Cambiar valor propiedad -> true / false
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    updateYearsInCompanyFilter(e) {
        console.log("updateYearsInCompanyFilter en persona-app");
        console.log("Se ha actualizado el filtro a " + e.detail.yearsInCompanyFilter);
        this.shadowRoot.querySelector("persona-main").yearsInCompanyFilter = e.detail.yearsInCompanyFilter;
    }

    // Función que se ejecuta al capturar el evento updated-people de persona-main
    updatedPeople(e) {
        console.log("updatedPeople en persona-app");
        console.log(e.detail);
        this.people = e.detail.people;
    }

    // Función que se ejecuta al capturar el evento updated-people-stats de persona-stats
    updatedPeopleStats(e) {
        console.log("updatedPeopleStats en persona-app");
        console.log(e.detail);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    }
}

customElements.define("persona-app", PersonaApp);