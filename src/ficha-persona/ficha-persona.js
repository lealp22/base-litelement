import { LitElement, html } from "lit-element";

class FichaPersona extends LitElement {

    static get properties() {
        return {
            fname: {type: String},
            yearsInCompany: {type: Number},
            persoInfo: {type: String}
        }
    }

    constructor() {
        super();

        this.fname = "Prueba nombre";
        this.yearsInCompany = 8;
        this.persoInfo = this.updatePersoInfo(this.yearsInCompany);
    }

    updated(changedProperties) {
        console.log(changedProperties);
        changedProperties.forEach((oldValue, propName) => {
            console.log("Ha cambiado el valor de la propiedad " + propName + " anterior era " + oldValue);
        });

        if (changedProperties.has("fname")) {
            console.log("Propiedad fname ha cambiado de valor, el anterior era " 
            + changedProperties.get("fname") + " nuevo es " + this.fname);
        }

        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany ha cambiado de valor, el anterior era " 
            + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
            this.persoInfo = this.updatePersoInfo(this.yearsInCompany);
        }
    }

    render() {
        return html`
            <div>
                <label>Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.fname}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text"  value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <label>PersoInfo</label>
                <input type="text"  value="${this.persoInfo}" disabled></input>
                <br />
            </div>
        `;
    }

    updateName(e) {
        console.log("updateName");

        this.fname = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany", e.target);

        this.yearsInCompany = e.target.value;
    }  
    
    updatePersoInfo(years) {

        console.log(years);
        var info = "";

        if (years >= 7) {
            info = "lead";
        } else if (years >= 5) {
            info = "senior";
        } else if (years >= 3) {
            info = "team";
        } else {
            info = "junior";
        }
        return info
    } 
}

customElements.define("ficha-persona", FichaPersona);